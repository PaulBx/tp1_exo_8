# Exercice 8

## Objectif
L'objectif de cet exercice est d'effectuer une intégration continue sous gitlab afin de vérifier le bon fonctionnement
du projet, et de permettre d'archiver la partie fonctionnelle du projet (Package_Calculator)

## Réalisation
Pour effectuer cela, on a ajouté un fichier .yml permettant de définir une série d'étape d'intégration continue
(vérification de la syntaxe, pytest, archivage).  
Pour que l'archivage se passe correctement il faut ajouter le chemin dans le fichier setup.py.

## Organisation
On a ajouté une étape d'arborescence (Calculator) pour le package du calculateur, afin de faciliter le chemin 
d'arborescence


## Gitlab
Le résultat de l'intégration continue est visible dans l'onglet CI_CD/pipeline du projet gitlab, on peut voir 
alors les trois étapes et s'ils elles ont bien été éxécutées sans problème.

  ![pipeline](images/pipeline.png)  
  
On voit donc sur la dernière sur le dernier commit que les trois étapes se sont passées sans problèmes, ainsi la syntaxe des programmes est bonne, les tests ont bien été effectué sans faute et l'archivage a été fait.
On peut également télécharger les archives composée du code utile (Package_Calculator) 

  ![archive1](images/archive1.png)  


## Ressource
TP sur l'intégration continue :
https://gitlab.com/js-ci-training/ci-hero-unitarytest-python